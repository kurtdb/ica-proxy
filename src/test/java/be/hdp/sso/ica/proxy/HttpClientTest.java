package be.hdp.sso.ica.proxy;

import be.hdp.sso.ica.proxy.exception.CitrixConnectException;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test to verify that the http client returns the correct response.
 */
public class HttpClientTest {

    private HttpClient client;

    @Before
    public void setUp() throws Exception {
        client = new HttpClient();
    }

    @Test
    public void testPerformHttpRequest() throws Exception {
        String result = client.performHttpRequest("test_linkasp1", "Partena007");
        Assert.assertFalse("The resulting ICA should be neither null nor empty", StringUtils.isEmpty(result));
        System.out.println(result);
    }

    @Test(expected = CitrixConnectException.class)
    public void testPerformHttpRequestWithInvalidCredentials() throws Exception {
        client.performHttpRequest("test_linkasp1", "Partena");
    }
}
