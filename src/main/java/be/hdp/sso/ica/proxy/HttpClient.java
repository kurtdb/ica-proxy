package be.hdp.sso.ica.proxy;

import be.hdp.sso.ica.proxy.exception.CitrixConnectException;
import be.hdp.sso.ica.proxy.util.PropertyNames;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.client.CookieStore;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.AuthSchemes;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Object mimicing that it's a clients browser performing http requests for an ICA-file.
 */
public class HttpClient {

    private static final Log LOG = LogFactory.getLog(HttpClient.class);

    private Properties properties;

    public HttpClient() {
        try {
            loadProperties();
        } catch (Exception e) {
            LOG.error("Error occured while initializing the http client", e);
        }
    }

    private void loadProperties() throws Exception {
        LOG.info("Starting to load properties");
        properties = new Properties();
        ClassLoader loader = this.getClass().getClassLoader();
        URL url = loader.getResource("ica-proxy.properties");
        if (url != null) {
            properties.load(url.openStream());
        } else {
            throw new IllegalArgumentException("Properties file (ica-proxy.properties) could not be loaded. Are you sure it is on the classpath?");
        }
        LOG.info("Properties loaded");
    }

    public String performHttpRequest(String username, String password) throws Exception {
        LOG.info("Starting request");
        String url = properties.getProperty(PropertyNames.CITRIX_SERVER_URL);

        try {
            HttpHost proxy = new HttpHost(properties.getProperty(PropertyNames.HTTP_PROXY_URL), Integer.valueOf(properties.getProperty(PropertyNames.HTTP_PROXY_PORT)), "http");
            DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
            RequestConfig requestConfig = RequestConfig.custom()
                    .setProxy(proxy)
                    .setProxyPreferredAuthSchemes(Arrays.asList(AuthSchemes.NTLM))
                    .setCookieSpec(CookieSpecs.BROWSER_COMPATIBILITY)
                    .build();
            BasicCookieStore cookieStore = new BasicCookieStore();
            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            credentialsProvider.setCredentials(AuthScope.ANY,
                    new NTCredentials(properties.getProperty(PropertyNames.HTTP_PROXY_USERNAME), properties.getProperty(PropertyNames.HTTP_PROXY_PASSWORD), "", ""));
            HttpClientBuilder builder = HttpClientBuilder.create()
                    .setDefaultCookieStore(cookieStore)
                    .setDefaultRequestConfig(requestConfig)
                    .setDefaultCredentialsProvider(credentialsProvider)
                    .setRoutePlanner(routePlanner);
            HttpClientContext context = HttpClientContext.create();
            context.setCookieStore(cookieStore);
            org.apache.http.client.HttpClient client = builder.build();
            url = url.trim();
            connectToLoginPage(client, context, url, 1);
            performLogon(client, context, url, username, password);
            performSetClient(client, context, url);
            followRedirect(client, context, url);
            getResourcesListForCookies(client, context, url);
            checkLogin(client, context, url);
            JSONObject resourceList = getExtendedResourcesList(client, context, url);
            String sessionId = getSessionId(resourceList);
            performSessionCheck(client, context, url, sessionId);

            return performLaunch(client, context, url, sessionId);
        } finally {
            LOG.info("Finishing request");
        }
    }

    private void connectToLoginPage(org.apache.http.client.HttpClient client, HttpClientContext context, String url, int attempt) throws Exception {
        HttpResponse response = null;
        String frontpage = properties.getProperty(PropertyNames.CITRIX_FRONT_PAGE);
        try {
            LOG.info("Connecting to the frontpage: " + url + frontpage);

            HttpGet request = getUnsecuredGet(url + frontpage);
            response = client.execute(request, context);
            LOG.info("HTTP return code: " + response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() == 403 && attempt < 3) {
                connectToLoginPage(client, context, url, attempt++);
            } else if (response.getStatusLine().getStatusCode() >= 400) {
                throw new CitrixConnectException("Connecting to logon page failed.");
            }
        } finally {
            if (response != null) {
                EntityUtils.consumeQuietly(response.getEntity());
            }
        }
    }

    private void performSetClient(org.apache.http.client.HttpClient client, HttpClientContext context, String url) throws Exception {
        HttpResponse response = null;
        String setClientPage = properties.getProperty(PropertyNames.CITRIX_SET_CLIENT_PAGE);
        try {
            LOG.info("Following redirect to " + url + setClientPage);
            HttpGet request = getUnsecuredGet(url + setClientPage);
            response = client.execute(request, context);
            LOG.info(response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() >= 400) {
                throw new CitrixConnectException("Performing the set client failed.");
            }
        } finally {
            if (response != null) {
                EntityUtils.consumeQuietly(response.getEntity());
            }
        }
    }

    private void performLogon(org.apache.http.client.HttpClient client, HttpClientContext context, String url, String username, String password) throws Exception {
        String logonPage = properties.getProperty(PropertyNames.CITRIX_LOGON_PAGE);
        String frontPage = properties.getProperty(PropertyNames.CITRIX_FRONT_PAGE);
        LOG.info("Trying to log in using the given credentials: " + url + logonPage);
        HttpResponse response = null;
        try {
            HttpPost request = getUnsecuredPost(url + logonPage);
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(properties.getProperty(PropertyNames.CITRIX_USERNAME_INPUT),
                    username));
            nameValuePairs.add(new BasicNameValuePair(properties.getProperty(PropertyNames.CITRIX_PASSWORD_INPUT), password));
            request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            response = client.execute(request, context);
            LOG.info(response.getStatusLine().getStatusCode());
            Header header = response.getFirstHeader("Location");

            if ((header != null && frontPage.equals(header.getValue())) || response.getStatusLine().getStatusCode() >= 400) {
                throw new CitrixConnectException("Performing the logon procedure failed.");
            }
        } finally {
            if (response != null) {
                EntityUtils.consumeQuietly(response.getEntity());
            }
        }
    }

    private void followRedirect(org.apache.http.client.HttpClient client, HttpClientContext context, String url) throws Exception {
        String redirectPage = properties.getProperty(PropertyNames.CITRIX_REDIRECT_PAGE);
        HttpResponse response = null;
        try {
            LOG.info("Following redirect to " + url + redirectPage);
            HttpGet request = getUnsecuredGet(url + redirectPage);
            response = client.execute(request, context);
            LOG.info(response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() >= 400) {
                throw new CitrixConnectException("Following the redirect failed.");
            }
        } finally {
            if (response != null) {
                EntityUtils.consumeQuietly(response.getEntity());
            }
        }
    }

    private void getResourcesListForCookies(org.apache.http.client.HttpClient client, HttpClientContext context, String url) throws Exception {
        String resourceListPage = properties.getProperty(PropertyNames.CITRIX_RESOURCE_LIST_PAGE);
        HttpResponse response = null;
        try {
            LOG.info("Getting resource list: " + url + resourceListPage);
            HttpPost request = getSecuredPost(context, url + resourceListPage);
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("format",
                    "json"));
            nameValuePairs.add(new BasicNameValuePair("resourceDetails", "Core"));
            request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            response = client.execute(request, context);
            LOG.info(response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() >= 400) {
                throw new CitrixConnectException("Trying to get the necessary cookies through the resource list failed.");
            }
        } finally {
            if (response != null) {
                EntityUtils.consumeQuietly(response.getEntity());
            }
        }
    }

    private JSONObject getExtendedResourcesList(org.apache.http.client.HttpClient client, HttpClientContext context, String url) throws Exception {
        String resourceListPage = properties.getProperty(PropertyNames.CITRIX_RESOURCE_LIST_PAGE);
        HttpResponse response = null;
        try {
            LOG.info("Getting extended resource list: " + url + resourceListPage);
            HttpPost request = getSecuredPost(context, url + resourceListPage);
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("format",
                    "json"));
            nameValuePairs.add(new BasicNameValuePair("resourceDetails", "Core"));
            request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            response = client.execute(request, context);
            LOG.info(response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() >= 400) {
                throw new CitrixConnectException("Getting the extended resource list failed.");
            }
            StringBuilder sb = new StringBuilder();
            if (response.getStatusLine().getStatusCode() == 200) {
                BufferedReader rd = new BufferedReader
                        (new InputStreamReader(response.getEntity().getContent()));

                String line;
                while ((line = rd.readLine()) != null) {
                    sb.append(line);
                    sb.append("\n");
                }
            }
            return new JSONObject(sb.toString());
        } finally {
            if (response != null) {
                EntityUtils.consumeQuietly(response.getEntity());
            }
        }
    }

    private void performSessionCheck(org.apache.http.client.HttpClient client, HttpClientContext context, String url, String sessionId) throws Exception {
        String launchStatusPage = properties.getProperty(PropertyNames.CITRIX_LAUNCH_STATUS_PAGE);
        HttpResponse response = null;
        try {
            LOG.info("Performing session check: " + url + launchStatusPage + sessionId);
            HttpPost request = getSecuredPost(context, url + launchStatusPage + sessionId);
            response = client.execute(request, context);
            LOG.info(response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() >= 400) {
                throw new CitrixConnectException("Performing the session check failed.");
            }
        } finally {
            if (response != null) {
                EntityUtils.consumeQuietly(response.getEntity());
            }
        }
    }

    private void checkLogin(org.apache.http.client.HttpClient client, HttpClientContext context, String url) throws Exception {
        String checkLoginPage = properties.getProperty(PropertyNames.CITRIX_CHECK_LOGIN_PAGE);
        HttpResponse response = null;
        try {
            LOG.info("Checking logon: " + url + checkLoginPage);
            HttpPost request = getSecuredPost(context, url + checkLoginPage);
            response = client.execute(request, context);
            LOG.info(response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() >= 400) {
                throw new CitrixConnectException("Performing the logon check failed.");
            }
        } finally {
            if (response != null) {
                EntityUtils.consumeQuietly(response.getEntity());
            }
        }
    }

    private String performLaunch(org.apache.http.client.HttpClient client, HttpClientContext context, String url, String sessionId) throws Exception {
        HttpResponse response = null;
        try {
            Cookie cookie = getCookieWithName(context, "CsrfToken");
            String launchPage = properties.getProperty(PropertyNames.CITRIX_PERFORM_LAUNCH_PAGE);
            launchPage = launchPage.replace("sessionId", sessionId);
            LOG.info("Performing launch: " + url + launchPage + cookie.getValue());
            HttpPost request = getSecuredPost(context, url + launchPage + cookie.getValue());
            response = client.execute(request, context);
            LOG.info(response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() >= 400) {
                throw new CitrixConnectException("Performing the launch failed.");
            }
            StringBuilder sb = new StringBuilder();
            if (response.getStatusLine().getStatusCode() == 200) {
                BufferedReader rd = new BufferedReader
                        (new InputStreamReader(response.getEntity().getContent()));

                String line;
                while ((line = rd.readLine()) != null) {
                    sb.append(line);
                    sb.append("\n");
                }
            }
            return sb.toString();
        } finally {
            if (response != null) {
                EntityUtils.consumeQuietly(response.getEntity());
            }
        }
    }

    private Cookie getCookieWithName(HttpClientContext context, String name) {
        CookieStore store = context.getCookieStore();
        List<Cookie> cookies = store.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(name)) {
                return cookie;
            }
        }
        return null;
    }

    private String getSessionId(JSONObject json) throws Exception {
        JSONArray resources = json.getJSONArray("resources");
        for (int i = 0; i < resources.length(); i++) {
            JSONObject obj = resources.getJSONObject(i);
            if ("HDPLink".equals(obj.getString("name"))) {
                return obj.getString("id");
            }
        }
        return null;
    }

    private HttpGet getUnsecuredGet(String url) {
        HttpGet request = new HttpGet(url);
        request.addHeader("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0");
        request.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        request.addHeader("Accept-Encoding", "gzip,deflate,sdch");
        request.addHeader("Accept-Language", "en-US,en;q=0.8");
        request.addHeader("Origin", url);
        request.addHeader("Referer", url + "/Citrix/LinkASPWeb/");
        request.addHeader("X-Requested-With", "XMLHttpRequest");
        return request;
    }

    private HttpPost getUnsecuredPost(String url) {
        HttpPost request = new HttpPost(url);
        request.addHeader("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0");
        request.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        request.addHeader("Accept-Encoding", "gzip,deflate,sdch");
        request.addHeader("Accept-Language", "en-US,en;q=0.8");
        request.addHeader("Origin", url);
        request.addHeader("Referer", url + "/Citrix/LinkASPWeb/");
        request.addHeader("X-Requested-With", "XMLHttpRequest");
        return request;
    }

    private HttpPost getSecuredPost(HttpClientContext context, String url) {
        HttpPost request = new HttpPost(url);
        request.addHeader("Content-Type", "application/x-www-form-urlencoded");
        request.addHeader("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0");
        request.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        request.addHeader("Accept-Encoding", "gzip,deflate,sdch");
        request.addHeader("Accept-Language", "en-US,en;q=0.8");
        request.addHeader("Origin", url);
        request.addHeader("Referer", url + "/Citrix/LinkASPWeb/");
        request.addHeader("X-Requested-With", "XMLHttpRequest");
        request.addHeader("X-Citrix-IsUsingHTTPS", "Yes");
        Cookie cookie = getCookieWithName(context, "CsrfToken");
        if (cookie != null) {
            request.addHeader("Csrf-Token", cookie.getValue());
        }
        return request;
    }
}
