package be.hdp.sso.ica.proxy.util;

/**
 * The property names for the settings in the ica-proxy.properties file.
 */
public class PropertyNames {

    public static final String CITRIX_SERVER_URL = "citrix.server.url";

    public static final String CITRIX_LOGON_PAGE = "citrix.logon.page";

    public static final String CITRIX_FRONT_PAGE = "citrix.front.page";

    public static final String CITRIX_REDIRECT_PAGE = "citrix.redirect.page";

    public static final String CITRIX_RESOURCE_LIST_PAGE = "citrix.resource.list.page";

    public static final String CITRIX_LAUNCH_STATUS_PAGE = "citrix.launch.status.page";

    public static final String CITRIX_CHECK_LOGIN_PAGE = "citrix.check.login.page";

    public static final String CITRIX_PERFORM_LAUNCH_PAGE = "citrix.perform.launch.page";

    public static final String CITRIX_SET_CLIENT_PAGE = "citrix.set.client.page";

    public static final String CITRIX_USERNAME_INPUT = "citrix.username.input";

    public static final String CITRIX_PASSWORD_INPUT = "citrix.password.input";

    public static final String HTTP_PROXY_URL = "http.proxy.url";

    public static final String HTTP_PROXY_PORT = "http.proxy.port";

    public static final String HTTP_PROXY_USERNAME = "http.proxy.username";

    public static final String HTTP_PROXY_PASSWORD = "http.proxy.password";
}
