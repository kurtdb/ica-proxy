package be.hdp.sso.ica.proxy.exception;

/**
 * Exception signalling that there was something wrong during the call to the citrix netscaler pages.
 */
public class CitrixConnectException extends Exception {
    public CitrixConnectException(String msg) {
        super(msg);
    }
}
